


WordPress is well-known for its ease of installation. Under most circumstances installing WordPress is a very simple process and takes less than five minutes to complete. Many web hosts now offer tools (e.g. Bluehost, mediatemple) to automatically install WordPress for you. 

However, if you wish to install WordPress yourself, you read about 

[Famous 5 Minute Installtion](https://codex.wordpress.org/Installing_WordPress#Famous_5-Minute_Install).


- [Things to know before you begin installing WordPress](https://codex.wordpress.org/Installing_WordPress#Things_to_Know_Before_Installing_WordPress)

- [The famous 5 minute installation](https://codex.wordpress.org/Installing_WordPress#Famous_5-Minute_Install)

- [Detailed installation instructions](https://codex.wordpress.org/Installing_WordPress#Detailed_Instructions)

- [Common installation problems](https://codex.wordpress.org/Installing_WordPress#Common_Installation_Problems)

- [Wordpress in your language](https://make.wordpress.org/polyglots/teams/).

- [Installing WordPress in your language](https://codex.wordpress.org/Installing_WordPress_in_Your_Language).

- [How to install multiple blogs](https://codex.wordpress.org/Installing_WordPress#Installing_Multiple_Blogs).

- [How to install on your own computer](https://codex.wordpress.org/).

- [Easy five minute WordPress installation on Windows](https://codex.wordpress.org/Installing_WordPress#Easy_5_Minute_WordPress_Installation_on_Windows).

- [Easy 5 minute Wordpress installation on Windows](https://codex.wordpress.org/Installing_WordPress#Easy_5_Minute_WordPress_Installation_on_Windows)

- [How to innstall with Amazon's Web Services (AWS)](https://codex.wordpress.org/Installing_WordPress#Installing_WordPress_at_AWS)

- [Getting started with Wordpress (much more installation information)](https://codex.wordpress.org/Getting_Started_with_WordPress)

!!! warning "IMPORTANT"


- **Some of the images included in the demo site are for showcase only and not distributed in the package.**

- **Server requirements: PHP 5.3 or greater, max_execution_time 120, memory_limit 64M, post_max_size 32M, upload_max_filesize 32M, allow_url_fopen ON.**

- Check to ensure that your web host has the minimum requirements to run WordPress.

- Always make sure they are running the latest version of WordPress.

- You can download the latest release of [**WordPress**](https://wordpress.org/) from official WordPress website.

- Always create secure passwords FTP and Database.


!!! note
Please notice that we do not give support or any questions related on how to install and maintain wordpress. For any faulty installation that may cause your website or our theme malfunction you should refer to wordpress support forum. Alternatively you can get help from your web hosting.



