


Along with the Customize, you can build page by using Visual composer. With Visual Composer you instantly have access to 40+ built in content elements available out of the box. 

##4.1. Install and activate Visual Composer

Make sure you have activated the Visual Composer plugin. This plugin is NOT built-into the theme (for good reasons) so it must be installed and activated as mentioned in the guide on “[Installing Recommended Plugins](https://wpexplorer-themes.com/total/docs/installing-recommended-plugins/)“. 
After installing the theme you should have been prompted to install the recommended plugins. 

This plugin is installed and activated, it gives you control over the layout of your pages.

![activate VC](assets/images/party-doc/vcactive.png)

##4.2. Visual Composer settings

Go to **Settings > Visual Composer** to setup the plugin. The most important part is to enable the composer for the post types you want to use it on.

![VC settings](assets/images/party-doc/vcsettings.png)

##4.3. Using Visual Composer

You have a look at the screenshots below for a quick guide

First, decide if you would like to use the Backend or Frontend editor and click the related button.

**1. The Visual Composer Backend Editor**

To work with Backend editor you first login to your WordPress admin pannel and navigate to Pages > Add new page.

![back end editor](assets/images/party-doc/backenoptions.png)

Visual Composer backend editor navigation consists of following options 

- **Visual Composer logo** : you can access to the creator's site vc.wpbakery.com hompage by clicking on this logo

- **Add new element** : add your favourite elements from Visual Composer elements library.

- **Template** : To add a default readymade template you can access to Visual Composer inbuilt default templates library and use template that matters your business.

![template](assets/images/party-doc/DefaultTemplates.png)

- **Frontend**: you can switch to Frontend from Backend, but you save your element before to switch to Frontend otherwise you will have to lose whatever you did on Backend.


If you click on the " + " symbol, the Visual Composer will open a window to add elements. Here you will find all of the Visual Composer and Total page building modules. To add a page element simply click on it.

![page template](assets/images/party-doc/pagelements.png)

Use the page element options to customize your page. Each element has different options for customizations like colors, fonts, margin, images and more. Below is a map of the backend editor with helpful pointers for the editor options.

![vc map](assets/images/party-doc/vcmap.png)

After adding elements make sure to save your page.

**Add element**

To add new element simply click on “Add new element” and choose “Row”

![add row](assets/images/party-doc/rows.png).

Now divide the Row in three columns by choosing custom columns or clicking on “Add column”

![add column](assets/images/party-doc/Choosecolumn2.png)

So you have now three columns now click on “Add element” and choose “Single image”

![single image](assets/images/party-doc/singleimage.png)

Now add an image and set size to “medium”

After that click on “Prepend to column”

![prepend column](assets/images/party-doc/prependcolumn.png)

Now choose “Text block” and add a new text that will appear after image

You can add the same image on other two rows by using clone option.

To clone image simply click on “Clone image”

![clone single image](assets/images/party-doc/Clonesingleimage.png)

Now drop the image on other column

![drop single image](assets/images/party-doc/Dropsingleimage.png)



You can also clone text block and drop on other two columns right below image

![text block row](assets/images/party-doc/textblockrow.png)

To add more elements, you always click on Add element button and choose any of your desired Visual Composer element.

![add more](assets/images/party-doc/addmoreelement.png)



Step 1. To add your page feature you click the **Add Element** button to open a pop-up menu. 

 There are lots of options for adding text boxes, headings, social sharing, images and galleries, toggles or tabs, accordions, recent posts (which also works with the custom post types of your theme in addition to the blog posts), buttons and even sidebar widgets.

Step 2. Click on any element to open the options for that element.

Below is an example of the “options” for our Icon Box module in the Total theme. As you can see each module can have many settings and even multiple tabs of settings.

![icon box](assets/images/party-doc/iconbox.png)

**2. The Visual Composer Front Editor** 

You may see this guide at link 

[Video guide to use Visual Composer Front Editor](https://www.youtube.com/watch?v=SOT_k4a-A50)


*This is video link to guide you use Visual Composer for building page.*

[https://vc.wpbakery.com/video-tutorials/](https://vc.wpbakery.com/video-tutorials/)



