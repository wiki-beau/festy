**How To Create Your Main Menu**

This steps are to create one page menu with multi page links

Step 1 – Go to the Appearance > Menus tab and create a new menu.

Step 2 – Build your menu as you normally would. For the links that you’d like to point to an anchor on a specific page, insert a ‘Custom Link’ and use the absolute link of the anchor. For example, if the anchor you’d like to point to is on the About page, then your URL will look like this: http://www.website.com/about#team

Step 3 – Keep repeating Step 2 until all your menu items that point to an anchor on another page all contain their respective absolute links.

Step 4 – Set this menu as your Main Navigation by checking the ‘Main Navigation’ checkbox

Step 5 – Save all your changes by clicking the ‘Save Menu’ button.

**How To Create Your Specific Menu**

Step 1 – Go to the Appearance > Menus tab and create a new menu. Give it a unique name.

Step 2 – Build your specific menu identical to your main menu. This time, instead of putting absolute links in the menu items that point to an anchor on another page, you’ll insert their relative link. For example, if the absolute link is http://www.website.com/about#team, then you’ll replace it with just #team as seen in in example image.

Step 3 – Keep repeating Step 2 until all your menu items that point to an anchor on another page all contain their respective relative links.

Step 4 – Save all your changes by clicking the ‘Save Menu’ button