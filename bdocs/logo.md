






There are two parts in this section so that you can change

In logo part : 

- Default logo : use available default logo

- Fixed header logo : select image you edit to set logo.

- Mobile logo : select image to set logo that displays on mobile.

![logo](assets/images/party-doc/logo.png)

In favicon part :

A favicon is a small icon that identifies a website in a web browser. Most browsers display a website's favicon in the left side of the address bar, next to the URL. Some browsers may also display the favicon in the browser tab, next to the page title. Favicons are automatically saved along with bookmarks or "favorites" as well.

- Apple Iphone icon : select images to set favicon on Iphone (size image 57 px x 57 px) 

- Apple Ipad icon : select images to set favicon on Ipad (size image 72 px x 72 px) 

You click Upload button to choose image from library.

![favicon](assets/images/party-doc/favicon.png)



 


















