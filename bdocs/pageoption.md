


In Dashboard, choose **Pages > Add new**.

Or you edit page by WP Visual Composer by clicking Backend Editor ( Frontend Editor ).

In each new  page, it will have page option part that consits of element

**Header** : 

![header](assets/images/party-doc/header.png)

**Footer** : select footer style that you wish

![footer](assets/images/party-doc/footer.png)

**Logo** : update logo that you want by clicking Add image > chooes file .

![logo](assets/images/party-doc/logopage.png)

**General**

You select sylesheet for page you already created.

![general page](assets/images/party-doc/pagegeneral.png)

Besides, you can use Page template by  choosing template in the right sidebar as you can see here 

![page template](assets/images/party-doc/template.png)
