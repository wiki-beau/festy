





!!! note "2. Theme option "


This is place  where to customize the whole theme.

In **Admin pannel** , you click  **Admin –> choose Theme option** . There will have a list of option which is displayed. You can edit it.  


##2.1. Header


You can update header styling  and header background.

In Dashboard, choose **Admin  >  Theme option  >  Header  >  Header styling**.

Towards Header styling :

- Header absolute : enable or disable it by switching on or off
If you choose the first header layout likely below picture, you could see its attributes including :
    
    - Enable sticky header : use sticky header

    - Header padding : change padding

    - Header shadow : use the shadow for the header

![header style](assets/images/party-doc/headstyle1.png)


In Dashboard, choose **Admin  >  Theme option  >  Header  >  Header background**.

You change header bckground image and setting its attributes

![header style](assets/images/party-doc/headstyle2.png)




##2.2. Social


In Dashboard, choose **Admin  >  Theme option  >  Social link** 

You can input your social link in blank as you view below picture :


![social](assets/images/party-doc/social.png)


##2.3. Logo

There are two parts in this section so that you can change

In logo part : 

- Default logo : use available default logo

- Fixed header logo : select image you edit to set logo.

- Mobile logo : select image to set logo that displays on mobile.

![logo](assets/images/party-doc/logo.png)

In favicon part :

A favicon is a small icon that identifies a website in a web browser. Most browsers display a website's favicon in the left side of the address bar, next to the URL. Some browsers may also display the favicon in the browser tab, next to the page title. Favicons are automatically saved along with bookmarks or "favorites" as well.

- Apple Iphone icon : select images to set favicon on Iphone (size image 57 px x 57 px) 

- Apple Ipad icon : select images to set favicon on Ipad (size image 72 px x 72 px) 

You click Upload button to choose image from library.

![favicon](assets/images/party-doc/favicon.png)

##2.4. General

In this part, you select main stylesheet for theme by clicking dropdown likely below picture :

![general](assets/images/party-doc/general.png)


##2.5. Typography

There are two parts so that you change typography for body and heading

Towards Body typography : you can set color and select its attributes

![typo](assets/images/party-doc/bodytypo.png)

Towards Heading typography : you change setting typography for heading 1 to heading 6.

![typo](assets/images/party-doc/headingtypo.png)

##2.6. Responsive

Turn on this feature to use the responsive design on mobile and other devices.

![responsive](assets/images/party-doc/responsive.png)

##2.7. Footer

- Footer content : 

![footer](assets/images/party-doc/footercontent.png)

- Footer background image :

![footer](assets/images/party-doc/footerbackground.png)

- Footer styling

![footer](assets/images/party-doc/footerstyle.png)


##2.8. Background 

You change body background image and its attribute including color, transparent, background repeat, background size, background attachment, background position.

![background](assets/images/party-doc/background.png)

##2.9. Custom CSS

This option allows you to input your own CSS

![CSS](assets/images/party-doc/css.png)


##2.10. Import/ Export 

 You export data > import it.

![export](assets/images/party-doc/import.png)
 

 


















