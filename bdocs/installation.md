

##1.1. Requirement

Our themes are compatible with the WordPress **versions 4.5.2** or higher ( PHP **5.2.4** or higher, and **mysql 5** or higher).
Most JavaScript enabled Browsers will make them work with no issues. They are designed and optimized for desktops, tablets and smartphones.


!!! note " Currently we are testing themes in"

- Google Chrome (Mac and PC)

- Firefox (Mac and PC)

- Internet Explorer 11 and later

- Safari (Mac and PC)


##1.2. Install theme


To install this theme you must have a working version of WordPress already installed. 

There are two method to install theme.

1. Method 1 : FTP upload

2. Method 2 : Wordpress upload

Step 1 : Navigate to **Appearance**

Step 2 : Click “ Add new theme ”

Step 3 : Click “Upload theme”

Step 4 : Choose the zipped theme folder

Step 5 : Click “Install now”

Step 6 : You see that notice “Theme is installed successfully” Iit means that work done.

Step 7 : After you install theme, you need to active theme by click link “Active” under notice “Theme is installed successfully” OR you can go to “Appearance > Theme > click Active button in installed theme“.


##1.3. Installed required plugins

You see that message informed all plugins need to be installed

- **Contact Form 7** : Used for contact form and newsletter sign up.

- **WPBakery page buider** : Page builder – This plugin will allow you to build your page easily.

- **Slider Revolution** : an innovative, responsive WordPress Slider Plugin that displays your content the beautiful way. 

- **WP Google Maps**

- **MailChimp**


!!! note
Please remember that all of plugins above is include with our theme. So you not need to purchase any more.
When you active this theme, you can download these plugins in the notice about requirement plugins in the top page

##1.4. Install Sample Data

Before you import sample data, you need to install and active Beautheme Core [Festy] to show "Theme options"

You can import our sample data by steps

Step 1 : Go to **Appearance > Import Demos**

Step 2 : Choose demo you want to import

Step 3 : Click on Import button .

Step 4 : Wait some minutes

Step 5 : Wait a moment and check site after you have finished importing demo.

You can view more on this video.

[https://streamable.com/cv88p](https://streamable.com/cv88p)




